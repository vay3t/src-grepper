package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var (
	DIR      string
	LIST_DIR []string
	BACKEND  string
	URL      string
	curlURL  string
)

func main() {

	flag.StringVar(&URL, "url", "https://localhost:8443/", "URL to Curl Creator")
	flag.StringVar(&DIR, "dir", "./burp-breaker/public_html", "Root directory to search")
	flag.StringVar(&BACKEND, "backend", "php", "Backend to search {php|py}")

	flag.Parse()

	enumerateFiles()
	//fmt.Println(1)
	for _, v := range LIST_DIR {
		//log.Println(v)
		searchOnly(v)

	}
}

func enumerateFiles() {
	os.Chdir(DIR)
	err := filepath.Walk(".",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() != true {
				//fmt.Println(path)
				LIST_DIR = append(LIST_DIR, path)
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
}

func searchOnly(path string) {
	//fmt.Println(path)
	if filepath.Ext(path) == "."+BACKEND {
		//fmt.Println(path)
		readFile(path)
	}
}

func readFile(path string) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	switch BACKEND {
	case "php":
		var parametersPOST []string
		var parametersGET []string
		var parametersREQ []string

		postReqRegex := regexp.MustCompile(`(?mU)\$_POST\[(.*)\]`)

		getReqRegex := regexp.MustCompile(`(?mU)\$_GET\[(.*)\]`)

		reqReqRegex := regexp.MustCompile(`(?mU)\$_REQUEST\[(.*)\]`)

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			// if PHP REGEX in scanner.Text()
			rePOST := postReqRegex.FindString(scanner.Text())
			reGET := getReqRegex.FindString(scanner.Text())
			reREQ := reqReqRegex.FindString(scanner.Text())

			if rePOST != "" {
				sanited := regexp.MustCompile(`("|')(.*)("|')`).FindString(rePOST)
				if sanited != "" {
					resPOST := strings.ReplaceAll(sanited, `"`, "")
					resPOST = strings.ReplaceAll(resPOST, `'`, "")
					parametersPOST = append(parametersPOST, resPOST)
				}
			}

			if reGET != "" {
				sanited := regexp.MustCompile(`("|')(.*)("|')`).FindString(rePOST)
				if sanited != "" {
					resGET := strings.ReplaceAll(sanited, `"`, "")
					resGET = strings.ReplaceAll(resGET, `'`, "")
					parametersGET = append(parametersGET, resGET)
				}
			}

			if reREQ != "" {
				sanited := regexp.MustCompile(`("|')(.*)("|')`).FindString(rePOST)
				if sanited != "" {
					resGET := strings.ReplaceAll(sanited, `"`, "")
					resGET = strings.ReplaceAll(resGET, `'`, "")
					parametersGET = append(parametersGET, resGET)
				}
			}

			if err := scanner.Err(); err != nil {
				log.Fatal(err)
			}
		}

		post_uniq := removeDuplicates(parametersPOST)
		get_uniq := removeDuplicates(parametersGET)
		req_uniq := removeDuplicates(parametersREQ)

		if len(req_uniq) > 0 {
			URL += "?"
			for _, v := range req_uniq {
				URL += v + "&"
			}
		}

		if len(get_uniq) > 0 {
			for _, v := range get_uniq {
				URL += "?" + v
			}
		}

		curlURL := "curl " + URL + path + " "
		if len(post_uniq) > 0 {
			for _, v := range post_uniq {
				curlURL += " -d " + v + "="
			}
		}

		fmt.Println(curlURL)

	case "py":
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			// if PY REGEX in scanner.Text()
			fmt.Println(scanner.Text())
		}
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	default:
		fmt.Println("No backend found")
		os.Exit(1)
	}
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func removeDuplicates(strList []string) []string {
	list := []string{}
	for _, item := range strList {
		//fmt.Println(item)
		if contains(list, item) == false {
			list = append(list, item)
		}
	}
	return list
}

/*
REGEX

$_GET[(.*)]
$_POST[(.*)]
$_REQUEST[(.*)]
$_COOKIE[(.*)]
$_SESSION[(.*)]
$_SERVER[('|")?HTTP_(.*)('|")?]

*/
